package com.dotscreen.java.tp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dotscreen.java.R;
import com.dotscreen.java.models.operator.Addition;
import com.dotscreen.java.models.operator.Division;
import com.dotscreen.java.models.operator.Multiplication;
import com.dotscreen.java.models.operator.Operator;
import com.dotscreen.java.models.operator.Subtraction;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

public class CalculatorActivity extends AppCompatActivity implements View.OnClickListener {

    TextView resultField, suiteField;

    Hashtable<Integer, Button> numbers = new Hashtable<>();

    Hashtable<String, Button> operators = new Hashtable<>();

    Hashtable<String, Button> actions = new Hashtable<>();

    boolean newNumber = true;
    boolean commaPressed = false;
    String previousOperator = "";
    Double result = (double) 0;
    String newStringNumber = "0";
    String computeOut = "";

    Operator addition, subtraction, division, multiplication;

    DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.getDefault()));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initOperators();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        df.setMaximumFractionDigits(340); //340 = DecimalFormat.DOUBLE_FRACTION_DIGITS

        for (int i = 0; i <= 9; i++) {
            try {
                int id = R.id.class.getField("cal_" + i).getInt(0);
                numbers.put(i, (Button) findViewById(id));
                numbers.get(i).setOnClickListener(this);
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
            }
        }

        actions.put("clear", (Button) findViewById(R.id.reset));
        actions.put("equal", (Button) findViewById(R.id.cal_equal));
        for (Button action : actions.values()) {
            action.setOnClickListener(this);
        }

        operators.put(addition.getName(), addition.getButton());
        operators.put(subtraction.getName(), subtraction.getButton());
        operators.put(multiplication.getName(), multiplication.getButton());
        operators.put(division.getName(), division.getButton());
        for (Button operator : operators.values()) {
            operator.setOnClickListener(this);
        }

        Button comma = (Button) findViewById(R.id.cal_dot);

        comma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!newStringNumber.contains(".")) {
                    commaPressed = true;
                    previousOperator = "add comma";
                    newStringNumber += '.';
                    resultField.setText(df.format(Double.valueOf(newStringNumber)));
                }
            }
        });

        resultField = (TextView) findViewById(R.id.cal_result);
        suiteField = (TextView) findViewById(R.id.cal_suite);
        suiteField.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void onClick(View view) {
        //if(actions.get("clear").equals(view))
        //    Snackbar.make(view,"OK",Snackbar.LENGTH_SHORT).show();
        if (view.getId() == R.id.reset) {
            onReset();
            return;
        } else if (view.getId() == addition.getIdView()) {
            compute(addition);
            return;
        } else if (view.getId() == subtraction.getIdView()) {
            compute(subtraction);
            return;
        } else if (view.getId() == division.getIdView()) {
            compute(division);
            return;
        } else if (view.getId() == multiplication.getIdView()) {
            compute(multiplication);
            return;
        } else if (view.getId() == R.id.cal_equal) {
            try {
                updateResult();
                newStringNumber = df.format(result);
                computeOut = "";
                suiteField.setVisibility(View.GONE);
                resultField.setText(df.format(result));
                newNumber = false;
                previousOperator = "equal";
            } catch (IllegalArgumentException e) {
                e.getMessage();
                onDivideException();
            }
            return;
        }

        Integer number;
        for (Map.Entry entry : numbers.entrySet()) {
            if (findViewById(view.getId()).equals(entry.getValue())) {
                //double test =0.0;
                if (previousOperator.equals("equal")) {
                    previousOperator = "add number";
                    newStringNumber = "0";
                    result = 0.0;
                }
                newNumber = true;
                number = (Integer) entry.getKey();
                if (newStringNumber.length() < 19)
                    computeInputValue(number);
                //Snackbar.make(view, "WorksD :" + String.format(Locale.getDefault(),"%f",Double.valueOf(newStringNumber)) +" "+newStringNumber.length(), Snackbar.LENGTH_SHORT).show();
                Snackbar.make(view, "WorksD :" + df.format(Double.valueOf(newStringNumber)) + " " + newStringNumber.length(), Snackbar.LENGTH_SHORT).show();
                resultField.setText(df.format(Double.valueOf(newStringNumber)));
                break; //breaking because its one to one map
            }
        }

        //Read more: http://javarevisited.blogspot.com/2013/02/how-to-get-key-from-value-in-hashtable.html#ixzz4U8umnZTE
        //Snackbar.make(view,"WorksD "+numbers.+" "+(view.getId()%10),Snackbar.LENGTH_SHORT).show();
        //Snackbar.make(view, "Result :" + df.format(result), Snackbar.LENGTH_SHORT).show();

    }

    private void computeInputValue(Integer input) {
        if ((!newStringNumber.contains(".")) && (newStringNumber.length() <= 14)) {
            double test;
            //(Math.floor(Double.valueOf("5"))==Double.valueOf("5"))
            test = Double.valueOf(newStringNumber);

            if (test == 0) {
                newStringNumber = input.toString();
            } else {
                newStringNumber += input;
            }
        } else
            newStringNumber += input;
    }

    private String replaceOperator(String string, char operator) {
        StringBuilder sb = new StringBuilder(string);
        sb.setCharAt(string.length() - 2, operator);
        return sb.toString();
    }

    private void updateResult() {
        double currentResult = result;
        switch (previousOperator) {
            case Operator.ADDITION:
                result += Double.valueOf(newStringNumber);
                onUpdateResult(currentResult);
                break;
            case Operator.SUBTRACTION:
                result -= Double.valueOf(newStringNumber);
                onUpdateResult(currentResult);
                break;
            case Operator.DIVISION:
                if (Double.valueOf(newStringNumber) == 0.0)
                    throw new IllegalArgumentException("Argument 'divisor' is 0");
                else
                    result /= Double.valueOf(newStringNumber);
                onUpdateResult(currentResult);
                break;
            case Operator.MULTIPLICATION:
                result *= Double.valueOf(newStringNumber);
                onUpdateResult(currentResult);
                break;
            default:
                break;
        }
    }

    private void initOperators() {
        addition = new Addition((Button) findViewById(R.id.plus), R.id.plus);
        subtraction = new Subtraction((Button) findViewById(R.id.minus), R.id.minus);
        division = new Division((Button) findViewById(R.id.divide), R.id.divide);
        multiplication = new Multiplication((Button) findViewById(R.id.cal_multiply), R.id.cal_multiply);
    }

    private void compute(Operator opr) {
        commaPressed = false;
        try {
            updateResult();
        } catch (IllegalArgumentException e) {
            e.getMessage();
            onDivideException();
        }
        if (newNumber) {
            if (opr instanceof Addition)
                result += Double.valueOf(newStringNumber);
            else if (opr instanceof Subtraction) {
                if (result != 0.0) result -= Double.valueOf(newStringNumber);
                else result = Double.valueOf(newStringNumber);
            } else if (opr instanceof Multiplication) {
                if (result != 0.0) result *= Double.valueOf(newStringNumber);
                else result = Double.valueOf(newStringNumber);
            } else if (opr instanceof Division) {
                if (result != 0.0) result /= Double.valueOf(newStringNumber);
                else result = Double.valueOf(newStringNumber);
            }
        }
        if (!newNumber && !previousOperator.equals(opr.getName()) && !previousOperator.equals("equal") && !previousOperator.equals("update")) {
            computeOut = replaceOperator(computeOut, opr.getSymbol());
        }
        previousOperator = opr.getName();
        newNumber = false;
        if (!newStringNumber.equals("0"))
            computeOut += newStringNumber + " " + opr.getSymbol() + " ";
        else if (computeOut.length() < 3)
            computeOut = "0" + " " + opr.getSymbol() + " ";
        suiteField.setText(computeOut);
        suiteField.setVisibility(View.VISIBLE);
        newStringNumber = "0";
        resultField.setText(df.format(result));
    }

    private void onUpdateResult(double currentResult) {
        if (currentResult != result)
            previousOperator = "update";
        newNumber = false;
    }

    private void onReset() {
        newStringNumber = "0";
        result = 0.0;
        resultField.setText("0");
        computeOut = "";
        previousOperator = "reset";
        suiteField.setVisibility(View.GONE);
    }

    private void onDivideException() {
        onReset();
        resultField.setTextColor(Color.RED);
        resultField.setText(R.string.dividedBy0Warning);
        resultField.setTextColor(Color.BLACK);
        Snackbar.make(this.findViewById(R.id.root_view), "On ne peut pas diviser par 0!", Snackbar.LENGTH_LONG).show();
    }

}
