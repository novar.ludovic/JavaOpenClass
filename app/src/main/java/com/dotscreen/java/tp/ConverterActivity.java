package com.dotscreen.java.tp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dotscreen.java.R;
import com.dotscreen.java.util.Calculator;

public class ConverterActivity extends AppCompatActivity {

    private EditText inputTemperature;
    private TextView outputCelsius;
    private TextView outputFahrenheit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conveter);

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        inputTemperature = (EditText) findViewById(R.id.inputTemp);
        outputCelsius = (TextView) findViewById(R.id.outputTempC);
        outputFahrenheit = (TextView) findViewById(R.id.outputTempF);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.convert_array,android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updateView(adapterView.getItemAtPosition(i).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        inputTemperature.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE //||
                        //actionId == EditorInfo.IME_ACTION_NEXT ||
                        //actionId == EditorInfo.IME_ACTION_GO ||
                        // event.getAction() == KeyEvent.ACTION_DOWN &&
                        // event.getKeyCode() == KeyEvent.KEYCODE_ENTER
                         ) {
                    //if (!event.isShiftPressed()) {
                        // the user is done typing.
                        updateView(String.valueOf(spinner.getItemAtPosition(spinner.getSelectedItemPosition())));
                        return true; // consume.
                    //}
                }
                return false;
            }
        });

    }

    private void updateView(String convertChoice){
        final double temp,inputD;
        final String input=inputTemperature.getText().toString();

        if(!input.equals(""))inputD = Double.parseDouble(input);
        else inputD =0.0;

        //inputD = Double.parseDouble(input);
        if(true) {
            switch (convertChoice) {
                case "Celsius":
                    temp = Calculator.convertToCelsius(inputD);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            outputFahrenheit.setText(inputD+"");
                            outputCelsius.setText(temp+"");
                            Toast.makeText(getApplicationContext(),temp+"", Toast.LENGTH_SHORT).show();
                        }
                    });

                    break;
                case "Fahrenheit":
                    temp = Calculator.convertToFahrenheit(inputD);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            outputCelsius.setText(inputD+"");
                            outputFahrenheit.setText(temp+"");
                            Toast.makeText(getApplicationContext(),temp+"", Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
            }
        }
    }
}
