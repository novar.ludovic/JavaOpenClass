package com.dotscreen.java.tp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.dotscreen.java.R;
import com.dotscreen.java.adapters.CustomListAdapter;
import com.dotscreen.java.adapters.GarageListAdapter;
import com.dotscreen.java.models.Garage;
import com.dotscreen.java.util.GarageLoad;

public class GarageActivity extends AppCompatActivity {

    private static final String GARAGE = "Garage" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_garage);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ListView mListView;
        GarageListAdapter adapter;
        Garage mGarage;

        mGarage= GarageLoad.garageLoad(getApplicationContext());
        Log.d(GARAGE, mGarage.toString());

        mListView = (ListView) findViewById(R.id.garageList);
        adapter = new GarageListAdapter(this, mGarage.getCars());

        mListView.setAdapter(adapter);

        //adapter.notifyDataSetChanged();

    }

}
