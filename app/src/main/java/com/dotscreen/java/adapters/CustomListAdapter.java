package com.dotscreen.java.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dotscreen.java.R;
import com.dotscreen.java.models.TP;

import java.util.List;

/**
 * Created by dotscreen on 14/12/2016.
 */

public class CustomListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<TP> tPItems;

    public CustomListAdapter(Activity activity, List<TP> tPItems) {
        this.activity = activity;
        this.tPItems = tPItems;
    }

    @Override
    public int getCount() {
        return tPItems.size();
    }

    @Override
    public Object getItem(int position) {
        return tPItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row, null);

        ImageView icon = (ImageView) convertView.findViewById(R.id.thumbnail);
        TextView title = (TextView) convertView.findViewById(R.id.title);

        // getting movie data for the row
        TP tp = tPItems.get(position);

        // thumbnail image
        icon.setImageResource(tp.getIcon());

        // title
        title.setText(tp.getTitle());

        return convertView;
    }
}
