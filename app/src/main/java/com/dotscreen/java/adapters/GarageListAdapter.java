package com.dotscreen.java.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dotscreen.java.R;
import com.dotscreen.java.models.TP;
import com.dotscreen.java.models.car.Car;
import com.dotscreen.java.models.car.Option;
import com.dotscreen.java.util.Calculator;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by dotscreen on 27/12/2016.
 */

public class GarageListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Car> carItems;

    public GarageListAdapter(Activity activity, List<Car> carItems) {
        this.activity = activity;
        this.carItems = carItems;
    }

    @Override
    public int getCount() {
        return carItems.size();
    }

    @Override
    public Object getItem(int position) {
        return carItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.garage_row, null);

        ImageView icon = (ImageView) convertView.findViewById(R.id.car);
        TextView mark = (TextView) convertView.findViewById(R.id.mark);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView engine = (TextView) convertView.findViewById(R.id.engine);
        TextView price = (TextView) convertView.findViewById(R.id.price);
        TextView pricevat = (TextView) convertView.findViewById(R.id.priceVAT);
        TextView optText = (TextView) convertView.findViewById(R.id.options);

        // getting movie data for the row
        Car car = carItems.get(position);

        String optString = listToString(car.getOptions());
        double carPrice = car.getEnginePrice()+car.getPrice();
        DecimalFormat df = new DecimalFormat("0.##");

        // thumbnail image
        icon.setBackgroundColor((int) (Math.random()*(Integer.MAX_VALUE)));

        // title
        mark.setText(" "+car.getMarkName().toString()+": ");
        name.setText(car.toString());
        engine.setText(car.getEngineCharacteristics());

        //double temp=Calculator.arrondi(carPrice,2);
        //price.setText(String.valueOf(temp)+"€");
        //pricevat.setText(String.valueOf(Calculator.arrondi(car.getPriceVAT(),2))+"€");
        price.setText(df.format(carPrice)+"€");
        pricevat.setText(df.format(car.getPriceVAT())+"€");
        optText.setText(optString);

        return convertView;
    }
    
    private String listToString(List<? extends Option> list){
        StringBuilder sb= new StringBuilder();
        sb.append("Options: \n");
        for (Option opt: list) {
            sb.append("  -")
            .append(opt.getClass().getSimpleName())
            .append(" (")
            .append(opt.getPrice())
            .append("€ )\n");
        }
        return sb.toString();
    }
}
