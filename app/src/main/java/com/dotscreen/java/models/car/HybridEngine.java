package com.dotscreen.java.models.car;

/**
 * Created by dotscreen on 27/12/2016.
 */

public class HybridEngine extends Engine {
    public HybridEngine(String power, double price) {
        super(power, price);
        setType(EngineType.HYBRID);
    }
}
