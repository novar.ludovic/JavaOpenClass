package com.dotscreen.java.models.car;

/**
 * Created by dotscreen on 27/12/2016.
 */

public interface Option {
    public double getPrice();
}
