package com.dotscreen.java.models.operator;

import android.widget.Button;

/**
 * Created by dotscreen on 29/12/2016.
 */

public class Division extends Operator {
    public Division(Button button, int idView) {
        super(button, idView);
        setName("divide");
        setSymbol('\u00f7');
    }

    public Division(Button button, int idView, char symbol, String name) {
        super(button, idView, symbol, name);
    }
}
