package com.dotscreen.java.models.operator;

import android.widget.Button;

/**
 * Created by dotscreen on 29/12/2016.
 */

public class Multiplication extends Operator {
    public Multiplication(Button button, int idView) {
        super(button, idView);
        setName("multiply");
        setSymbol('\u00d7');
    }

    public Multiplication(Button button, int idView, char symbol, String name) {
        super(button, idView, symbol, name);
    }
}
