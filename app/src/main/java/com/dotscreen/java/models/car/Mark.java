package com.dotscreen.java.models.car;

/**
 * Created by dotscreen on 27/12/2016.
 */

public enum Mark {
    Renault("Renault"),
    Peugeot("Peugeot"),
    Citroen("Citroen");

    private String name = "";

    Mark(String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }
}
