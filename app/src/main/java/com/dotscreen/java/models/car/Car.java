package com.dotscreen.java.models.car;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dotscreen on 27/12/2016.
 */

public class Car implements Serializable{
    protected double price;
    private String name;
    private List<Option> options;
    private Mark markName;
    private Engine engine;

    public double getPrice() {
        return price;
    }

    public double getPriceVAT() {
        double optPrice=0;
        for (Option opt :getOptions()) {
            optPrice+=opt.getPrice();
        }
        return price+getEnginePrice()+optPrice;
    }

    public void addOption(Option opt){
        options.add(opt);
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setMarkName(Mark markName) {
        this.markName = markName;
    }

    public Mark getMarkName() {
        return markName;
    }

    public String toString(){
        return this.getClass().getSimpleName();
    }

    public void setEngine(Engine engine){
        this.engine=engine;
    }

    public String getEngineCharacteristics() {
        return engine.toString();
    }

    public double getEnginePrice() {
        return engine.getPrice();
    }


    Car(Mark mark) {
        this.options = new ArrayList<>();
        setMarkName(mark);
    }
}
