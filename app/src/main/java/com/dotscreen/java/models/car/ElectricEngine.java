package com.dotscreen.java.models.car;

/**
 * Created by dotscreen on 27/12/2016.
 */

public class ElectricEngine extends Engine {
    public ElectricEngine(String power, double price) {
        super(power, price);
        setType(EngineType.ELECTRIC);
    }
}
