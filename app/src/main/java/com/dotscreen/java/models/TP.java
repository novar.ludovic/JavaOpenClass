package com.dotscreen.java.models;

/**
 * Created by dotscreen on 14/12/2016.
 */

public class TP {

    private String title;
    private int icon;

    public TP(String name, int iconId) {
        this.title = name;
        this.icon = iconId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public int getIcon() {
        return icon;
    }

}
