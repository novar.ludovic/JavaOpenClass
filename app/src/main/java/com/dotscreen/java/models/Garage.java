package com.dotscreen.java.models;

import com.dotscreen.java.models.car.Car;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dotscreen on 27/12/2016.
 */

public class Garage implements Serializable{
    private List<Car> cars;

    public Garage() {
        this.cars = new ArrayList<>();
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public void addCar(Car car){
        cars.add(car);
    }

    @Override
    public String toString() {
        return "En cours";
    }
}
