package com.dotscreen.java.models.operator;

import android.widget.Button;

/**
 * Created by dotscreen on 29/12/2016.
 */

public abstract class Operator {
    public static final String ADDITION="plus";
    public static final String SUBTRACTION="minus";
    public static final String DIVISION="divide";
    public static final String MULTIPLICATION="multiply";
    private String name;
    private Button button;
    private int idView;
    private char symbol;

    public Operator() {

    }

    public Operator(Button button,int idView) {
        this.idView = idView;
        this.button = button;
    }

    public Operator(Button button, int idView, char symbol, String name) {
        this.name = name;
        this.button = button;
        this.idView = idView;
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    public int getIdView() {
        return idView;
    }

    public void setIdView(int idView) {
        this.idView = idView;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }
}
