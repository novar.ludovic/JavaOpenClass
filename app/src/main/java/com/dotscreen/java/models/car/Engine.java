package com.dotscreen.java.models.car;

import java.io.Serializable;

/**
 * Created by dotscreen on 27/12/2016.
 */

abstract class Engine implements Serializable {
    private EngineType type;
    private String power;
    private double price;

    Engine(String power, double price) {
        this.power = power;
        this.price = price;
    }

    public void setType(EngineType type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public String toString() {
        return "Moteur "+this.type.toString()+" "+power;
    }
}

