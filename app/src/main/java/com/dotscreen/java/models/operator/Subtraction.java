package com.dotscreen.java.models.operator;

import android.widget.Button;

/**
 * Created by dotscreen on 29/12/2016.
 */

public class Subtraction extends Operator {
    public Subtraction(Button button, int idView) {
        super(button, idView);
        setName("minus");
        setSymbol('-');
    }

    public Subtraction(Button button, int idView, char symbol, String name) {
        super(button, idView, symbol, name);
    }
}
