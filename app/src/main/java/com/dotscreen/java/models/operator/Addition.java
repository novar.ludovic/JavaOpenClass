package com.dotscreen.java.models.operator;

import android.widget.Button;

/**
 * Created by dotscreen on 29/12/2016.
 */

public class Addition extends Operator {
    public Addition(Button button,int idView) {
        super(button,idView);
        setName("plus");
        setSymbol('+');
    }

    public Addition(Button button, int idView, char symbol, String name) {
        super(button, idView, symbol, name);
    }
}
