package com.dotscreen.java.models.car;

/**
 * Created by dotscreen on 27/12/2016.
 */

public enum EngineType {
    DIESEL("Diesel"),
    ESSENCE("Essence"),
    HYBRID("Hybride"),
    ELECTRIC("Electrique");

    private String name = "";

    EngineType(String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }
}
