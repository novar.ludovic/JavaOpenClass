package com.dotscreen.java.models.car;

/**
 * Created by dotscreen on 27/12/2016.
 */

public class GasolineEngine extends Engine {
    public GasolineEngine(String power, double price) {
        super(power, price);
        setType(EngineType.ESSENCE);
    }
}
