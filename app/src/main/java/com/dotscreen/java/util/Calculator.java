package com.dotscreen.java.util;

/**
 * Created by dotscreen on 15/12/2016.
 */

public class Calculator {

    /**
     * Cette fonction permet de convertir une temperature donner en degre celsius et de retourner une temperature en degré fahrenheit.
     * La valeur est arrondi à l'aide de la méthode
     * @see #arrondi
     * @param temp une temperature donner en degre celsius
     * @return une temperature en degré fahrenheit
     */
    public static double convertToFahrenheit(double temp){
        return arrondi((9.0/5.0)*temp+32.0,2);
    }

    public static double convertToCelsius(double temp){
        return arrondi(((temp-32)*5)/9,2);
    }

    /**
     * Permet de definir le nombre de chiffre après la virgule d'un double
     * @param A double à arrondir <code>null</code>
     * @param B nombre de chiffre après la virgule
     * @return chiffre arrondi
     */

    public static double arrondi(double A, int B) {
        return (double) ( (int) (A * Math.pow(10, B) + .5)) / Math.pow(10, B);
    }
}
