package com.dotscreen.java.util;

import android.content.Context;
import android.util.Log;

import com.dotscreen.java.models.Garage;
import com.dotscreen.java.models.car.A300B;
import com.dotscreen.java.models.car.AirConditioning;
import com.dotscreen.java.models.car.Car;
import com.dotscreen.java.models.car.D4;
import com.dotscreen.java.models.car.DieselEngine;
import com.dotscreen.java.models.car.ElectricEngine;
import com.dotscreen.java.models.car.GPS;
import com.dotscreen.java.models.car.GasolineEngine;
import com.dotscreen.java.models.car.HeatedSeat;
import com.dotscreen.java.models.car.HybridEngine;
import com.dotscreen.java.models.car.Lagouna;
import com.dotscreen.java.models.car.PowerWindow;
import com.dotscreen.java.models.car.RoofBar;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by dotscreen on 27/12/2016.
 */

public class GarageLoad {
    /**
     *  Cette methode charge et sauvegarde un nouveau garage s'il n'en trouve pas dans le stockage interne de l'application
     *
     * https://openclassrooms.com/courses/creez-des-applications-pour-android/le-stockage-de-donnees-2
     * @param appContext Context de l'application afin d'accéder à l'espace de stockage interne de l'application
     * @return un garage de voiture
     */
    public static Garage garageLoad(Context appContext) {
        Garage garage = new Garage();
        boolean newData = true;

        try(FileInputStream fis = appContext.openFileInput("myGarage.txt");
            BufferedInputStream bis=new BufferedInputStream(fis);
            ObjectInputStream ois = new ObjectInputStream(bis)){
            newData=false;
            garage = (Garage) ois.readObject();
            ois.close();
        }
        catch (FileNotFoundException e){
            Log.d("Garage","Le fichier n'existe pas");
        }
        catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        if(newData){
            Log.d("Garage","C'est un nouveau garage");
            Car lag1 = new Lagouna();
            lag1.setEngine(new GasolineEngine("150 Chevaux", 10256d));
            lag1.addOption(new GPS());
            lag1.addOption(new HeatedSeat());
            lag1.addOption(new PowerWindow());
            garage.addCar(lag1);

            Car A300B_2 = new A300B();
            A300B_2.setEngine(new ElectricEngine("1500 W", 1234d));
            A300B_2.addOption(new AirConditioning());
            A300B_2.addOption(new RoofBar());
            A300B_2.addOption(new HeatedSeat());
            garage.addCar(A300B_2);

            Car d4_1 = new D4();
            d4_1.setEngine(new DieselEngine("200 Hdi", 25684.80d));
            d4_1.addOption(new RoofBar());
            d4_1.addOption(new AirConditioning());
            d4_1.addOption(new GPS());
            garage.addCar(d4_1);

            Car lag2 = new Lagouna();
            lag2.setEngine(new DieselEngine("500 Hdi", 456987d));
            garage.addCar(lag2);

            Car A300B_1 = new A300B();
            A300B_1.setEngine(new HybridEngine("ESSENCE/Electrique", 12345.95d));
            A300B_1.addOption(new PowerWindow());
            A300B_1.addOption(new RoofBar());
            garage.addCar(A300B_1);

            Car d4_2 = new D4();
            d4_2.setEngine(new ElectricEngine("100 KW", 1224d));
            d4_2.addOption(new HeatedSeat());
            d4_2.addOption(new RoofBar());
            d4_2.addOption(new AirConditioning());
            d4_2.addOption(new GPS());
            d4_2.addOption(new PowerWindow());
            garage.addCar(d4_2);
            //FileOutputStream fods = null;

            Car d4_3 = new D4();
            d4_3.setEngine(new ElectricEngine("110 KW", 4000d));
            d4_3.addOption(new HeatedSeat());
            d4_3.addOption(new RoofBar());
            d4_3.addOption(new AirConditioning());
            d4_3.addOption(new GPS());
            d4_3.addOption(new PowerWindow());
            garage.addCar(d4_3);

            try(FileOutputStream fos = appContext.openFileOutput("myGarage.txt",MODE_PRIVATE);
                BufferedOutputStream bos=new BufferedOutputStream(fos);
                ObjectOutputStream oos = new ObjectOutputStream(bos)){
                oos.writeObject(garage);
                // http://stackoverflow.com/questions/18054184/closing-a-nested-stream-closes-its-parent-streams-too
                // http://stackoverflow.com/questions/884007/correct-way-to-close-nested-streams-and-writers-in-java
                // When closing chained streams, you only need to close the outermost stream. Any errors will be propagated up the chain and be caught.
                oos.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else Log.d("Garage","C'est un vieux garage");
        return garage;
    }
}
